import { NgModule } from '@angular/core';

import {TodosComponent} from './components/todos/todos.component';
import {RouterModule, Routes} from '@angular/router';
import {RemindersComponent} from './components/reminders/reminders.component';

const routes: Routes = [
  { path: '',   redirectTo: 'todos', pathMatch: 'full' },
  { path: 'todos', component: TodosComponent },
  { path: 'reminders', component: RemindersComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
