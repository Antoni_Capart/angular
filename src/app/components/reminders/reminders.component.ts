import {Component, OnInit} from '@angular/core';
import {dataOperations, jexiaClient} from 'jexia-sdk-js/browser';
import {environment} from '../../../environment';
import {FormBuilder} from "@angular/forms";

const dataModule = dataOperations();
jexiaClient().init(environment.jexia, dataModule);

const reminders = dataModule.dataset('reminders');

@Component({
  templateUrl: './reminders.component.html',
})
export class RemindersComponent implements OnInit {

  checkedReminders = [];
  uncheckedReminders = [];

  checkoutForm;

  constructor(private formBuilder: FormBuilder) {
    this.checkoutForm = this.formBuilder.group({
      todo: '',
      date: new Date(),
    });
  }

  ngOnInit(): void {
    this.update();
  }

  onSubmit(value: any): void {
    reminders
      .insert({
        todo: value.todo,
        completed: false,
        date: value.date,
      })
      .subscribe(
        () => console.log('Insertion Ok !'),
        () => console.log('Insertion Bad !')
      );

    this.update();
    this.checkoutForm.reset();
  }

  checkReminder(id: string, bool: boolean): void {
    reminders
      .update({completed: bool})
      .where(t => t('id').isEqualTo(id))
      .subscribe(() => this.update());
  }

  update(): void {
    reminders
      .select()
      .subscribe(
        result => {
          this.checkedReminders = result.filter(t => t.completed);
          this.uncheckedReminders = result.filter(t => !t.completed);
        }
      );
  }

  delete(id: string): void {
    reminders
      .delete()
      .where(t => t('id').isEqualTo(id))
      .subscribe(() => this.update());
  }
}
