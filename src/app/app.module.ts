import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {TodosComponent} from './components/todos/todos.component';
import {AppRoutingModule} from './routing.module';
import {AppComponent} from './app.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RemindersComponent} from './components/reminders/reminders.component';

@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    RemindersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
